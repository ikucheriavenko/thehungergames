<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CarWash - Express</title>
    <link rel="shortcut icon" href="/web/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/web/css/reset.css">
    <link rel="stylesheet" href="/web/css/bootstrap.css">
    <link rel="stylesheet" href="/web/css/template-style.css">
    <link rel="stylesheet" href="/web/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">
    <style>
        <?php include 'web/css/'.$content_style; ?>
    </style>
</head>
<body>
<header>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo-hidden" href="/">Dashboard</a>
                <a class="navbar-brand logo" href="/"><img src="/web/img/logo.jpg" alt="dashboard" width="50" height="50"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left">
                    <li><a class="menu" href="/cars">My Cars</a></li>
                    <?php $url = explode('/', $_SERVER['REQUEST_URI']);
                        if (($url[1] == 'dashboard') && (!isset($data['services']))) {?>
                        <li id="my-service">My Services</li>
                    <?php } else { ?>
                    <li><a class="menu" href="/services">My Services</a></li>
                    <?php } ?>
                    <li><a class="menu" href="/contacts">Contact Support</a></li>
                </ul>
                <ul class="right-menu nav navbar-nav navbar-right">
                    <li id="userpic"><img  src="/web/img/userpic.png" alt=""></li>
                    <li class="dropdown open">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php if (!empty($_SESSION)) echo $_SESSION['user'] ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/profile">Profile</a></li>
                            <li><a href="/logout">Log Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>
</header>
<script src="/web/js/jquery-2.1.3.min.js"></script>
<script src="/web/js/bootstrap.js"></script>

    <?php include 'application/views/'.$content_view; ?>

</body>
</html>
