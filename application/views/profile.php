<section>
    <div class="container">
        <h4 class="text-center">My profile</h4>
        <div>
            <p class="text-center">Fill your data</p>
            <p class=" text-center small">Note:  use only real information about yourself.
            Any false information can led to problems and future inconvenience</p>
        </div>
        <div class="text-center" id="error-msg"><?php if(isset($data)) echo $data;?></div>
        <div class="edit-profile">
            <form name="profile" action="/profile" enctype="multipart/form-data" onsubmit="return emailValidation()"  method="post">
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-4 col-xs-offset-1 col-sm-offset-0 part">
                        <div>
                            <label for="uploader">Userpic</label>
                            <input name="uploader" type="file" id="uploader" accept="image/jpeg,image/png" onchange="previewImg(this)">
                        </div>
                        <div>
                            <label for="name">Name</label>
                            <input name="name" type="text" id="name" onkeydown="limitLength(this,20);"
                                   onkeyup="limitLength(this,20);">
                        </div>
                        <div>
                            <label for="phone">Phone</label>
                            <input type="text" id="phone" value="<?php echo $_SESSION['user'] ?>" readonly>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-3 text-center img-wrapper">
                        <img id="preview" src="http://placehold.it/50x50" alt="#fff" width="50" height="50"/>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-4 part">
                        <div>
                            <label for="last-name">Last name</label>
                            <input name="last_name" type="text" id="last-name" onkeydown="limitLength(this,20);"
                                   onkeyup="limitLength(this,20);">
                        </div>
                        <div>
                            <label for="email">Email</label>
                            <input name="email" type="email" id="email" onkeydown="limitLength(this,30);"
                                   onkeyup="limitLength(this,30);">
                        </div>
                        <div>
                            <label for="address">Address</label>
                            <input name="address" type="text" id="address">
                        </div>
                    </div>
                    <div class="navigation">
                        <a class="btn btn-info left" href="/">Back</a>
                        <button class="btn-add btn btn-info right" type="submit">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!--    <script>-->
<!--        previewImg();-->
<!--    </script>-->
    <script src="/web/js/profile.js"></script>
</section>