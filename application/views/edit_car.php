<section>
    <div class="container">
        <p>Fill information about your car</p>
        <div id="error-msg"><?php if(isset($data['error'])) echo $data['error'];?></div>
        <div class="add-car">
            <form name="add_new_car" action="/cars/edit" onsubmit="return validateCar();" method="post">
                <input id="data_from_server" type="hidden" value='<?php echo $data['cars_list'];?>'>
                <input name="car_id" id="car_id" type="hidden" value="<?php echo $data['car_for_edit']['id']?>">
                <input name="user_car_id" type="hidden" value="<?php echo $data['car_for_edit']['user_car_id']?>">
                <div class=" part left-part">
                    <div class="drop" id="mark-drop">
                        <label for="mark">Mark</label>
                        <select name="mark" id="mark" onmousedown="resetData()" onchange="openNextSelect(null,this,'model');">
                            <option value="<?php echo $data['car_for_edit']['mark']?>">
                                <?php echo $data['car_for_edit']['mark']?>
                            </option>
                        </select><br />
                    </div>
                    <div class="drop" id="model-drop" style="display: block">
                        <label for="model">Model</label>
                        <select name="model" id="model" onmousedown="resetData()" onchange="openNextSelect('mark',this,'year');">
                            <option value="<?php echo $data['car_for_edit']['model']?>">
                                <?php echo $data['car_for_edit']['model']?>
                            </option>
                        </select>
                    </div>
                    <div>
                        <label for="plate">Reg. plate</label>
                        <input name="plate" type="text" id="plate" maxlength="10" value="<?php echo $data['car_for_edit']['plate']?>">
                    </div>
                </div>
                <div class="part right-part">
                    <div class="drop" id="year-drop">
                        <label for="year">Year</label>
                        <select name="year" id="year" onmousedown="resetData()" onchange="openNextSelect('model',this,'type');">
                            <option value="<?php echo $data['car_for_edit']['year']?>">
                                <?php echo $data['car_for_edit']['year']?>
                            </option>
                        </select>
                    </div>
                    <div class="drop" id="type-drop">
                        <label for="type">Body Type</label>
                        <select name="type" id="type" onmousedown="resetData()" onchange="openNextSelect('year',this,null);">
                            <option value="<?php echo $data['car_for_edit']['type']?>">
                                <?php echo $data['car_for_edit']['type']?>
                            </option>
                        </select>
                    </div>
                    <button class="btn-add btn btn-success" type="submit">Edit</button>
                </div>
            </form>
        </div>
    </div>
    <script src="/web/js/edit-car.js"></script>
    <script src="/web/js/add-car.js"></script>
    <script src="/web/js/car-validation.js"></script>
</section>