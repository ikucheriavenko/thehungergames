<?php $slide_number = null; ?>
<section>
    <div class="container">
        <input id="error" type="hidden" value="<?php if(isset( $_GET['error'])) echo $_GET['error']; ?>">
        <div class="title-wrapper">
            <h3 id="title">List of rendered services</h3>
            <a href="/services/newOrder" class="btn btn-success right">Buy Service</a>
        </div>
        <div id="_all" class="carousel slide" data-ride="carousel" data-interval="false">
            <div class="carousel-inner" role="listbox">
                <?php if ($data != null) { ?>
                <?php $slide_number = ((int)(count($data)/10)+1); for ($k = 0; $k < $slide_number; $k++) { ?>
                    <div class="item">
                        <table>
                            <tr>
                                <th>Date</th>
                                <th>Service Type</th>
                                <th>Car</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                            <?php
                            foreach($data as $row) {
                                echo '<tr><td>'.$row['date'].'</td><td>'.$row['description'].'</td><td>'.
                                    $row['mark'].' '.$row['model'].'</td><td>'.' $'.$row['price'].'</td>
                                    <td><a href="/services/edit?id='.$row['id'].'">Edit</a>
                                        <a href="/services/cancel?id='.$row['id'].'">Cancel</a></td></tr>';
                            }
                            ?>
                        </table>
                    </div>
                    <?php $data = array_slice($data,10); } ?>
                <?php } else { ?>
                    <p class="text-center">Hello, if you registered your car you can order a service</p>
                <?php }?>
            </div>
            <!-- Left and right controls -->
            <?php if ($slide_number > 1) { ?>
            <a id="prev" class="left carousel-control btn btn-primary" href="#_all" role="button" data-slide="prev">
                Previous
            </a>
            <a id="next" class="right carousel-control btn btn-primary" href="#_all" role="button" data-slide="next">
                Next
            </a>
            <?php } ?>
        </div>
    </div>
    <div class="modal" id="cancel-edit-error" >
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="modal-title" id="myModalLabel">Validation error</h5>
                </div>
                <div class="modal-body">
                    <div id="modal-message"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</section>
<!--bootstrap class active for viewing first carousel's slide-->
<script>
    var items = document.getElementsByClassName("item");
    items[0].className = items[0].className + " active";
    // manage modal window
    if(document.getElementById('error').value === '1') {
        document.getElementById('modal-message').innerHTML =
        "Sorry, you can't delete/edit this service. It remains less than 2 hours";
        $('#cancel-edit-error').modal('toggle');
    }
</script>