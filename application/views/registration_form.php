<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Registration</title>
    <link rel="stylesheet" href="/web/css/bootstrap.css">
    <link rel="stylesheet" href="/web/css/form-style.css">
    <link rel="stylesheet" href="/web/css/register-style.css">
</head>
<body>
    <div class="login">
        <h3>Sign Up</h3>
        <div id="error-msg">
            <?php if ($data != null) echo $data; ?>
        </div>
        <form name="register" action="/registration" onsubmit="return validateForm(this);" method="post">
            <input name="phone" type="text" placeholder="8(xxx)xxx-xx-xx"/>
            <input name="password" type="password" placeholder="Password" onkeydown="limitLength(this,30)"/>
            <button class="btn btn-success" type="submit">Sign up</button>
        </form>
    </div>
    <script src="/web/js/user-validation.js"></script>
</body>
</html>
