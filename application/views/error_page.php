<?php
    $code = $data[0];
    $message = $data[1];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $message;?></title>
    <link rel="stylesheet" href="/web/css/error-style.css">
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
</head>
<body>
    <div class="code"><?php echo $code; ?></div>
    <p>Houston, we have a problem...</p>
    <p><?php echo $message; ?></p>
    <a href="/">May be better I'll come back</a>
</body>
</html>