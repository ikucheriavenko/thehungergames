<?php
// extract news
$news_title = array();
$text = array();
foreach ($data['news'] as $key => &$value) {
    array_push($news_title,$key);
    array_push($text,$value);
}
?>

<section>
    <div class="container">
        <div class="wrapper section">
        <?php if (!isset($data['cars'])) {?>
            <div class="error-msg" >
            <i class="fa fa-times-circle" ></i >
            <span>You have no cars added yet. Please open <a href="/cars">My Cars</a>
                section to add a new car. Then you will be ready to start using our
                services.</span >
        </div >
        <?php } else {?>
            <h4>Latest Cars</h4>
            <table>
                <tr>
                    <th>Date Added</th>
                    <th>Body Type</th>
                    <th>Car Description</th>
                    <th>Action</th>
                </tr>
                <?php
                foreach($data['cars'] as $row) {
                echo '<tr><td>'.$row['date'].'</td><td>'.$row['type'].'</td><td>'.
                        $row['mark'].' '.$row['model'].'</td>
                        <td><a href="/cars">View</a></td></tr>';
                }
                ?>
            </table>
        <?php } ?>
        <?php if (!isset($data['services']) && (isset($data['cars']))) {?>
            <div class="error-msg" >
                <i class="fa fa-times-circle" ></i >
                <span>There is no service added yet for your car(s).<a href="/services">Buy service</a>
                right now to fill all the advantages of our website</span >
            </div >
        <?php } ?>
        <?php if (isset($data['services']))  {?>
            <h4>Latest Services</h4>
            <table>
                <tr>
                    <th>Date</th>
                    <th>Service</th>
                    <th>Car</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                <?php
                    foreach($data['services'] as $row) {
                        echo '<tr><td>'.$row['date'].'</td><td>'.$row['description'].'</td><td>'.
                            $row['mark'].' '.$row['model'].'</td><td>'.' $'.$row['price'].'</td>
                            <td><a href="#">View</a></td></tr>';
                    }
                ?>
            </table>
        <?php } ?>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="wrapper">
            <h3>news</h3>
            <div class="items">
                <div class="item">
                    <img class="img-valign" src="http://www.placehold.it/50x50" alt="#" />
                    <div class="text">
                        <h4><?php echo $news_title[0] ?></h4><br>
                        <span><?php echo $text[0] ?></span><br>
                        <a href="#">more..</a>
                    </div>
                </div>
                <div class="item">
                    <img class="img-valign" src="http://www.placehold.it/50x50" alt="#" />
                    <div class="text">
                        <h4><?php echo $news_title[1] ?></h4><br>
                        <span><?php echo $text[1] ?></span><br>
                        <a href="#">more..</a>
                    </div>
                </div>
                <div class="item">
                    <img class="img-valign" src="http://www.placehold.it/50x50" alt="#" />
                    <div class="text">
                        <h4><?php echo $news_title[2] ?></h4><br>
                        <span><?php echo $text[2] ?></span><br>
                        <a href="#">more..</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>