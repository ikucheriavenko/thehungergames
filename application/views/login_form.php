<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title>Log in</title>
	<link rel="stylesheet" href="/web/css/bootstrap.css">
	<link rel="stylesheet" href="/web/css/form-style.css">
</head>
<body>
	<div class="login">
		<h3>Log in</h3>
		<div id="error-msg">
			<?php if ($data != null) echo $data; ?>
		</div>
	  	<form name="login" action="/login" onsubmit="return validateForm(this);" method="post">
	    	<input name="phone" type="text" placeholder="8(xxx)xxx-xx-xx"/>
	    	<input name="password" type="password" placeholder="Password" onkeydown="limitLength(this,30)"/>
	    	<div class="button-wrapper">
	    		<button class="btn btn-primary" type="submit">Log in</button>
	    		<a class="btn btn-success" href="/registration">Sign up</a>
	    	</div>
	  	</form>
	</div>
	<script src="/web/js/user-validation.js"></script>
</body>
</html>
	