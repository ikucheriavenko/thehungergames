<section>
    <div class="container">
        <h4>Select comfortable time</h4>
        <p class="text">Your order: "<?php echo $data['about_service']['service_name']?>"</p>
        <p class="text small">(<?php echo $data['about_service']['description']?>)</p>
        <div id="error-msg"><?php if(isset($data['errors'])) echo $data['errors'];?></div>
        <div class="order">
            <form name="new_service" action="/services/saveOrder" onsubmit=" return validate()" method="post">
                <input name="service_id"  type="hidden" value='<?php echo $data['service_id'] ?>'>
                <input name="date" type="hidden" value='<?php echo $data['date'] ?>'>
                <input name="user_car_id" type="hidden" value='<?php echo $data['user_car_id'] ?>'>
                <!--  using for editing service. it's id of corrected service  -->
                <input name="user_service_id" type="hidden"
                       value="<?php if (isset($_POST['user_service_id'])) echo $_POST['user_service_id'];?>">
                <input id="time_schedule" type="hidden" value='<?php echo $data['time'] ?>'>
                <div class="left-part">
                    <div>
                        <label class="title" for="time-select">Start time</label>
                        <select name="time" id="time-select" onchange="getEndTime()">
                            <option value="">-- no chosen --</option>
                        </select><br />
                    </div>
                    <div>
                        <span>End time:</span>
                        <span id="end-time"></span>
                    </div>
                    <div>
                        <span>Price:</span>
                        <span><?php echo '$'.$data['price'] ?></span>
                    </div>
                </div>
                <div class="navigation">
                    <a id="back" href="/services/newOrder" class="btn btn-success">Back</a>
                    <button id="next" class="btn btn-success" type="submit">Next</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal" id="validation-error" >
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="modal-title" id="myModalLabel">Validation error</h5>
                </div>
                <div class="modal-body">
                    <div id="modal-message"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <script src="/web/js/add-service-time.js"></script>
</section>