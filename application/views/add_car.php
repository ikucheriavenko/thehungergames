<section>
    <div class="container">
        <p>Fill information about your car</p>
        <div id="error-msg"><?php if(isset($data['error'])) echo $data['error'];?></div>
        <div class="add-car">
            <form name="add_new_car" action="/cars/add" onsubmit="return validateCar();" method="post">
                <input id="data_from_server" type="hidden" value='<?php echo $data['cars_list'];?>'>
                <input name="car_id" id="car_id" type="hidden" value="">
                <div class=" part left-part">
                    <div class="drop" id="mark-drop">
                        <label for="mark">Mark</label>
                        <select name="mark" id="mark"  onchange="openNextSelect(null,this,'model');">
                            <option value="">-- no chosen --</option>
                        </select><br />
                    </div>
                    <div class="drop" id="model-drop" style="display: none">
                        <label for="model">Model</label>
                        <select name="model" id="model" onchange="openNextSelect('mark',this,'year');">
                            <option value="">-- no chosen --</option>
                        </select>
                    </div>
                    <div>
                        <label for="plate">Reg. plate</label>
                        <input name="plate" type="text" id="plate" maxlength="10">
                    </div>
                </div>
                <div class="part right-part">
                    <div class="drop" id="year-drop" style="display: none">
                        <label for="year">Year</label>
                        <select name="year" id="year" onchange="openNextSelect('model',this,'type');">
                            <option value="">-- no chosen --</option>
                        </select>
                    </div>
                    <div class="drop" id="type-drop" style="display: none">
                        <label for="type">Body Type</label>
                        <select name="type" id="type" onchange="openNextSelect('year',this,null);">
                            <option value="">-- no chosen --</option>
                        </select>
                    </div>
                    <button class="btn-add btn btn-success" type="submit">Add</button>
                </div>
            </form>
        </div>
    </div>
    <script src="/web/js/add-car.js"></script>
    <script src="/web/js/car-validation.js"></script>
    <script>init();</script>
</section>