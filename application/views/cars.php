<?php $slide_number = null; ?>
<section>
    <div class="container">
        <div class="title-wrapper">
            <h3 id="title">List of your added cars</h3>
            <a href="/cars/add" class="btn btn-success right">Add car</a>
        </div>
        <div id="_all" class="carousel slide" data-ride="carousel" data-interval="false">
            <div class="carousel-inner" role="listbox">
                <?php if ($data != null) {?>
                <?php $slide_number = ((int)(count($data)/10)+1); for ($k = 0; $k < $slide_number; $k++) { ?>
                        <div class="item">
                            <table>
                                <tr>
                                    <th>Date</th>
                                    <th>Body Type</th>
                                    <th>Car</th>
                                    <th>Action</th>
                                </tr>
                                <?php
                                foreach($data as $row) {
                                    echo '<tr><td>'.$row['date'].'</td><td>'.$row['type'].'</td><td>'.
                                        $row['mark'].' '.$row['model'].'</td>
                                        <td><a href="/cars/edit?id='.$row['id'].'">Edit</a>
                                        <a href="/cars/delete?id='.$row['id'].'">Delete</a>
                                        <a href="/services/newOrder?car_id='.$row['id'].'">Get Service</a></td></tr>';
                                }
                                ?>
                            </table>
                        </div>
                <?php $data = array_slice($data,10); } ?>
                <?php } else { ?>
                    <p class="text-center">Hello, please add your cars and we'll start work!</p>
                <?php }?>
            </div>
            <!-- Left and right controls -->
            <?php if ($slide_number > 1) {?>
            <a id="prev" class="left carousel-control btn btn-primary" href="#_all" role="button" data-slide="prev">
                Previous
            </a>
            <a id="next" class="right carousel-control btn btn-primary" href="#_all" role="button" data-slide="next">
                Next
            </a>
            <?php }?>
        </div>
    </div>
</section>
<!--bootstrap class active for viewing first carousel's slide-->
<script>
    var items = document.getElementsByClassName("item");
    items[0].className = items[0].className + " active";
</script>







