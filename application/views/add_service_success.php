<section>
    <div class="container">
        <p class="text-uppercase text-center">Thank you!</p>
        <p class="text-center small">You can drop your order in "my service",
            but not later than 2 hours before service start.</p>
        <div class="text-center">
            <a class="btn btn-success " href="/services">My Services</a>
        </div>
    </div>
</section>