<section>
    <div class="container">
        <p>Choose service you need:</p>
        <div id="error-msg"><?php if(isset($data['errors'])) echo $data['errors'];?></div>
        <div class="buy-service">
            <form name="new_service" action="/services/chooseTime" onsubmit="return dateValidate()" method="post">
                <input id="user_cars" type="hidden" value='<?php echo $data['user_cars'];?>'>
                <input id="services" type="hidden" value='<?php echo $data['services'];?>'>
                <!-- use it for editing service  -->
                <input name="user_service_id" type="hidden" value='<?php if(isset($_GET['id'])) echo $_GET['id']?>'>
                <!-- use it when user call 'get service' for his car from cars list -->
                <input type="hidden" id="car_id" value='<?php if(isset($_GET['car_id'])) echo $_GET['car_id']?>'>
                <div class="left-part">
                    <div class="drop" id="service-drop">
                        <label for="service-select">Service Type</label>
                        <select name="service_id" id="service-select" >
                            <option value="">-- no chosen --</option>
                        </select><br />
                    </div>
                    <div>
                        <label for="date">Date</label>
                        <input name="date" type="text" id="date" maxlength="10" data-toggle="tooltip"
                            data-placement="right"  title="ex: 25/08/2015" placeholder="dd/mm/yyyy">
                    </div>
                    <div class="drop" id="car-drop">
                        <label for="car-select">Car</label>
                        <select name="type_id/user_car_id" id="car-select">
                            <option value="">-- no chosen --</option>
                        </select>
                    </div>
                </div>
                <div class="navigation">
                    <a id="back" href="/services" class="btn btn-success">Back</a>
                    <button id="next" class="btn btn-success" type="submit">Next</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal" id="validation-error" >
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="modal-title" id="myModalLabel">Validation error</h5>
                </div>
                <div class="modal-body">
                    <div id="modal-message"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('document').ready(function(){
            $('#date').tooltip();
        });
    </script>
    <script src="/web/js/add-service.js"></script>
    <script src="/web/js/service-validation.js"></script>
</section>