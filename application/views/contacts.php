<section>
    <div class="container">
        <h4>Hey! Contact us! We appreciate it!</h4>
        <p>Note: you can add some file if you think we must see it :)</p>
        <div id="error-msg"><?php if(isset($data)) echo $data; ?></div>
        <div id="contacts">
            <form name="contacts" enctype="multipart/form-data" action="/contacts" onsubmit="return checkFeedback()"
                  method="post">
                <div class=" part left-part">
                    <div>
                        <label for="uploader">Attachment</label>
                        <input name="uploader" type="file" id="uploader">
                    </div>
                    <div>
                        <label for="subject">Subject</label>
                        <input name="subject" type="text" id="subject" onkeydown="limitLength(this);" onkeyup="limitLength(this);">
                    </div>
                </div>
                <div class="part right-part">
                    <span id="title-msg">Message</span>
                    <textarea name="message" id="area-msg" placeholder="Message" rows=3" cols="31"></textarea>
                    <button class="btn-add btn btn-info" type="submit">Send</button>
                </div>
            </form>
        </div>
    </div>
    <script src="/web/js/contacts-validation.js"></script>
</section>