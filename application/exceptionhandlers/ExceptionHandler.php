<?php

class ExceptionHandler {

    private static $host;

    public function catchAllException($e) {
        self::$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        if ($e instanceof PageNotFoundException) {
            header('Location:'.self::$host.'error/notFound');
        } else {
            header('Location:'.self::$host.'error/internalException');
        }
    }
}