<?php

class NewsRepositoryImpl implements NewsRepository {

    private $connection;

    function __construct() {
        $this->connection = DBConnection::Instance()->getConnection();
    }

    public function get($ids) {
        $sql  = "SELECT title, text FROM news WHERE id = ?";
        $stmt = $this->connection->prepare($sql);
        $size = count($ids);
        $result = array();
        for ($i = 0; $i < $size; $i++)  {
            $stmt->bind_param("i", $ids[$i]);
            if (!$stmt->execute()) {
                throw new InternalServerException("Execute failed: ({$stmt->errno}) {$stmt->error}");
            }
            $stmt->bind_result($title,$text);
            $stmt->fetch();
            $result[$title] = $text;
        }
        $stmt->close();
        return $result;
    }


}