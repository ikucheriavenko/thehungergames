<?php

class UserRepositoryImpl implements UserRepository {
    private $connection;

    function __construct() {
        $this->connection = DBConnection::Instance()->getConnection();
    }

    public function add(User $user) {
        $query = "INSERT INTO users (phone, password) VALUES (?, ?)";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("ss", $user->getPhone(), HashUtil::getHash($user->getPassword()));
        if (!$statement->execute()) {
            throw new InternalServerException("Execute failed: ({$statement->errno}) {$statement->error}");
        }
        $statement->close();
    }

    public function get($phone, $password) {
        $query = "SELECT first_name, last_name, email, avatar
                                                 FROM users WHERE phone=? and password=?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("ss",$phone, HashUtil::getHash($password));
        if (!$statement->execute()) {
            throw new InternalServerException("Execute failed: ({$statement->errno}) {$statement->error}");
        }
        $statement->bind_result($first_name, $last_name, $email, $avatar);
        $statement->store_result();
        if ($statement->num_rows > 0) {
            $user = new User();
            $user->setPhone($phone);
            while ($statement->fetch()) {
                $user->setFirstName($first_name);
                $user->setLastName($last_name);
                $user->setEmail($email);
                $user->setAvatar($avatar);
            }
            $statement->free_result();
            $statement->close();
            return $user;
        } else {
            return null;
        }
    }

    public function getId($phone) {
        $query = "SELECT id FROM users WHERE phone=?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("s",$phone);
        if (!$statement->execute()) {
            throw new InternalServerException("Execute failed: ({$statement->errno}) {$statement->error}");
        }
        $statement->bind_result($id);
        $statement->store_result();
        if ($statement->num_rows > 0) {
            $statement->fetch();
            $statement->free_result();
            $statement->close();
            return $id;
        }
        return null;
    }

    public function addUserMessage($subject, $message, $user_id, $file) {
        $query = "INSERT INTO user_files (subject, message, user_id, file) VALUES (?, ?, ?, ?)";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("ssis", $subject, $message, $user_id, $file);
        if (!$statement->execute()) {
            throw new InternalServerException("Execute failed: ({$statement->errno}) {$statement->error}");
        }
        $statement->close();
    }

    public function update($id, $first_name, $last_name, $email, $address, $avatar) {
        $query = "UPDATE users SET first_name=?, last_name=?, email=?, avatar=?, address=? WHERE id=?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("sssssi", $first_name, $last_name, $email, $avatar, $address, $id);
        if (!$statement->execute()) {
            throw new InternalServerException("Execute failed: ({$statement->errno}) {$statement->error}");
        }
        $statement->close();
    }

}