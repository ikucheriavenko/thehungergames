<?php

class CarRepositoryImpl implements CarRepository{

    private $connection;

    function __construct() {
        $this->connection = DBConnection::Instance()->getConnection();
    }

    function get($user_phone) {
        $query = "SELECT c.mark,c.model,tp.id,tp.type,u_c.id,u_c.added FROM cars c
                    JOIN types tp ON tp.id = c.type_id
                    JOIN user_cars u_c ON c.id = u_c.car_id
                    JOIN users u ON u.id = u_c.user_id
                    WHERE phone=? ORDER BY u_c.id DESC";
            $stmt = $this->connection->prepare($query);
            $stmt->bind_param("s",$user_phone);
            if (!$stmt->execute()) {
                throw new InternalServerException("Execute failed: ({$stmt->errno}) {$stmt->error}");
            }
            $stmt->bind_result($mark, $model, $type_id, $type, $id, $added);
            $result = array();
            $stmt->store_result();
            if ($stmt->num_rows > 0) {
                while ($stmt->fetch()) {
                    $car = array();
                    $car['id'] = $id;
                    $car['mark'] = $mark;
                    $car['model'] = $model;
                    $car['type_id'] = $type_id;
                    $car['type'] = $type;
                    $car['date'] = $added;
                    array_push($result, $car);
                }
                $stmt->free_result();
                $stmt->close();
                return $result;
            } else {
                return null;
            }
    }

    function getAll() {
        $query = "SELECT c.id, c.mark, c.model, c.year,tp.type FROM cars c
                        JOIN types tp ON c.type_id = tp.id";
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            $rows = array();
            while($row = $result->fetch_assoc()) {
                $rows[] = $row;
            }
        } else {
            throw new InternalServerException();
        }
        return $rows;
    }


    function add($user_id, $car_id, $plate, $date) {
        $query = "INSERT INTO user_cars (user_id, car_id, plate, added) VALUES (?, ?, ?, ?)";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("iiss",$user_id, $car_id, $plate, $date);
        if (!$statement->execute()) {
            throw new InternalServerException("Execute failed: ({$statement->errno}) {$statement->error}");
        }
        $statement->close();
    }

    function delete($id) {
        $query = "DELETE from user_cars WHERE id=?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("i",$id);
        if (!$statement->execute()) {
            throw new InternalServerException("Execute failed: ({$statement->errno}) {$statement->error}");
        }
        $statement->close();
    }

    function getById($user_car_id) {
        $query = "SELECT c.id,c.mark,c.model,c.year, tp.type,u_c.plate FROM cars c
                    JOIN types tp ON tp.id = c.type_id
                    JOIN user_cars u_c ON c.id = u_c.car_id
                    JOIN users u ON u.id = u_c.user_id
                  WHERE u_c.id = ?;";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("s",$user_car_id);
        if (!$stmt->execute()) {
            throw new InternalServerException("Execute failed: ({$stmt->errno}) {$stmt->error}");
        }
        $stmt->bind_result($id, $mark, $model,$year, $type, $plate);
        $result = array();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            while ($stmt->fetch()) {
                $result ['user_car_id'] = $user_car_id;
                $result['id'] = $id;
                $result['mark'] = $mark;
                $result['model'] = $model;
                $result['year'] = $year;
                $result['type'] = $type;
                $result['plate'] = $plate;
            }
            $stmt->free_result();
            $stmt->close();
            return $result;
        } else {
            return null;
        }
    }

    function update($user_car_id, $car_id, $plate, $date) {
        $query = "UPDATE user_cars SET car_id=?, plate=?, added=? WHERE id=?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("issi",$car_id, $plate, $date, $user_car_id);
        if (!$stmt->execute()) {
            throw new InternalServerException("Execute failed: ({$stmt->errno}) {$stmt->error}");
        }
        $stmt->close();
    }

}