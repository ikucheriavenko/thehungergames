<?php

class SessionRepositoryImpl implements SessionRepository {

    private $connection;

    function __construct() {
        $this->connection = DBConnection::Instance()->getConnection();
    }

    function deleteOld($time) {
        $query = sprintf("DELETE FROM sessions WHERE timestamp < %s", $time);
        return $this->connection->query($query);
    }

    function get($id) {
        $query = sprintf("SELECT * FROM sessions WHERE id = '%s'",
                            $this->connection->escape_string($id));
        if ($result = $this->connection->query($query)) {
            if ($result->num_rows && $result->num_rows > 0) {
                $record = $result->fetch_assoc();
                return $record['data'];
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    function update($id, $data) {
        if (isset($_SESSION['user'])) {
            $query = sprintf("REPLACE INTO sessions VALUES('%s', '%s', '%s')",
                $this->connection->escape_string($id),
                $this->connection->escape_string($data),
                time());
            return $this->connection->query($query);
        }
       return true;
    }

    function delete($id) {
        $query = sprintf("DELETE FROM sessions WHERE id = '%s'",
                        $this->connection->escape_string($id));
        $this->connection->query($query);
        return true;
    }
}