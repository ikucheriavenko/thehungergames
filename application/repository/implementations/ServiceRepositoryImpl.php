<?php

class ServiceRepositoryImpl implements ServiceRepository {

    private $connection;

    function __construct() {
        $this->connection = DBConnection::Instance()->getConnection();
    }

    function get($user_phone) {
        $query = "SELECT serv_pr.price, c.mark, c.model, us_serv.id, us_serv.date, serv.description
                  FROM service_prices serv_pr
                        JOIN services serv ON serv.id = serv_pr.service_id
                        JOIN user_services us_serv ON serv_pr.service_id = us_serv.service_id
                        JOIN user_cars us_ca ON us_ca.id =  us_serv.user_car_id
                        JOIN cars c ON c.id = us_ca.car_id AND c.type_id = serv_pr.type_id
                        JOIN users u ON u.id = us_ca.user_id
                  WHERE u.phone=? ORDER by us_serv.id DESC";

        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("s",$user_phone);
        if (!$stmt->execute()) {
            throw new InternalServerException("Execute failed: ({$stmt->errno}) {$stmt->error}");
        }
        $stmt->bind_result($price, $mark, $model, $id, $date, $description);
        $stmt->store_result();
        $result = array();
        if ($stmt->num_rows > 0) {
            while ($stmt->fetch()) {
                $service = array();
                $service['price'] = $price;
                $service['mark'] = $mark;
                $service['model'] = $model;
                $service['id'] = $id;
                $service['date'] = $date;
                $service['description'] = $description;
                array_push($result, $service);
            }
            $stmt->free_result();
            $stmt->close();
            return $result;
        } else {
            return null;
        }
    }

    function purge($user_car_id) {
        $query = "DELETE from user_services WHERE user_car_id=?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("i",$user_car_id);
        if (!$statement->execute()) {
            throw new InternalServerException("Execute failed: ({$statement->errno}) {$statement->error}");
        }
        $statement->close();
    }


    function getAll() {
        $query = "SELECT id, service_name FROM services";
        $result = $this->connection->query($query);
        if ($result->num_rows > 0) {
            $rows = array();
            while($row = $result->fetch_assoc()) {
                $rows[] = $row;
            }
        } else {
            throw new InternalServerException();
        }
        return $rows;
    }

    function getPrice($service_id, $type_id) {
        $query = "SELECT price FROM service_prices WHERE service_id=? AND type_id=?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("ii",$service_id, $type_id);
        if (!$stmt->execute()) {
            throw new InternalServerException("Execute failed: ({$stmt->errno}) {$stmt->error}");
        }
        $stmt->bind_result($price);
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->fetch();
            $stmt->free_result();
            $stmt->close();
            return $price;
        } else {
            return null;
        }
    }

    function getTimes($date) {
        $query = "SELECT time FROM user_services WHERE date=?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("s",$date);
        if (!$stmt->execute()) {
            throw new InternalServerException("Execute failed: ({$stmt->errno}) {$stmt->error}");
        }
        $stmt->bind_result($time);
        $stmt->store_result();
        $result = array();
        if ($stmt->num_rows > 0) {
            while ($stmt->fetch()) {
                array_push($result,$time);
            }
            $stmt->free_result();
            $stmt->close();
            return $result;
        } else {
            return null;
        }
    }

    function getService($id) {
        $query = "SELECT service_name, description FROM services WHERE id=?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("i",$id);
        if (!$stmt->execute()) {
            throw new InternalServerException("Execute failed: ({$stmt->errno}) {$stmt->error}");
        }
        $stmt->bind_result($service_name, $service_description);
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->fetch();
            $result = array('service_name' => $service_name,
                            'description'=> $service_description);
            $stmt->free_result();
            $stmt->close();
            return $result;
        } else {
            return null;
        }
    }

    function save($service_id, $date, $user_car_id, $time) {
        $query = "INSERT INTO user_services (user_car_id, service_id, time, date) VALUES (?, ?, ?, ?)";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("iiss",$user_car_id, $service_id, $time, $date);
        if (!$statement->execute()) {
            throw new InternalServerException("Execute failed: ({$statement->errno}) {$statement->error}");
        }
        $statement->close();
    }

    function getUserServiceTime($id) {
        $query = "SELECT time, date FROM user_services WHERE id=?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("i",$id);
        if (!$stmt->execute()) {
            throw new InternalServerException("Execute failed: ({$stmt->errno}) {$stmt->error}");
        }
        $stmt->bind_result($time, $date);
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            $stmt->fetch();
            $result = array('time' => $time,
                            'date' => $date);
            $stmt->free_result();
            $stmt->close();
            return $result;
        } else {
            return null;
        }
    }

    function delete($id) {
        $query = "DELETE FROM user_services WHERE id=?";
        $stmt = $this->connection->prepare($query);
        $stmt->bind_param("i",$id);
        if (!$stmt->execute()) {
            throw new InternalServerException("Execute failed: ({$stmt->errno}) {$stmt->error}");
        }
        $stmt->close();
    }

    function update($id, $service_id, $date, $user_car_id, $time) {
        $query = "UPDATE carwash.user_services SET user_car_id=?, service_id=?, time=?, date=? WHERE id=?";
        $statement = $this->connection->prepare($query);
        $statement->bind_param("iissi", $user_car_id, $service_id, $time, $date, $id);
        if (!$statement->execute()) {
            throw new InternalServerException("Execute failed: ({$statement->errno}) {$statement->error}");
        }
        $statement->close();
    }
}