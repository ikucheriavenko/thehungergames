<?php

interface SessionRepository {
    function deleteOld($time);
    function get($id);
    function update($id, $data);
    function delete($id);
}