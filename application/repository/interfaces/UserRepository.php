<?php

interface UserRepository {
    public function add(User $user);
    public function get($phone, $password);
    public function getId($phone);
    public function addUserMessage($subject, $message, $user_id, $file);
    public function update($id, $first_name, $last_name, $email, $address, $avatar);
}