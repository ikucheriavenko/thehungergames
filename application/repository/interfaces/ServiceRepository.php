<?php

interface ServiceRepository {
    function get($user_phone);
    function purge($user_car_id);
    function getAll();
    function getPrice($service_id, $type_id);
    function getTimes($date);
    function getService($id);
    function save($service_id, $date, $user_car_id, $time);
    function getUserServiceTime($id);
    function delete($id);
    function update($id, $service_id, $date, $user_car_id, $time);
}