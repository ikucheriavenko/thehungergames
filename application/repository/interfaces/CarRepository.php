<?php

interface CarRepository {
    function get($user_phone);
    function getById($id);
    function getAll();
    function add($user_id, $car_id, $plate, $date);
    function delete($id);
    function update($user_car_id, $car_id, $plate, $date);
}