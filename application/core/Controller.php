<?php

abstract class Controller {
	
	public $model;
	public $view;
	
	function __construct() {
		$this->view = new View();
	}

   	abstract function actionIndex();

    public function redirect($url){
        header("location: {$url}");
        exit();
    }
}
