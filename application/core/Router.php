<?php
ini_set('display_errors', 1);

class Router {

	private static $defaultController = 'LoginController';
    private static $defaultAction = 'actionIndex';

	public static function start() {
        $routes = explode('/', $_SERVER['REQUEST_URI']);
        $controller = self::getController($routes);
        $action = self::getAction($routes);

        // if user isn't authorized - go to login page or registr. page
        if (empty($_SESSION) && ($controller != 'RegistrationController')) {
            $controller = null;
            $action = null;
        }

        if (null === $controller) {
            $controller = self::$defaultController;
        }
        if (null === $action) {
            $action = self::$defaultAction;
        }

        $controller = new $controller;
		
		if(method_exists($controller, $action))  {
			$controller->$action();
		} else {
			throw new PageNotFoundException();
		}
	}

    private static function getController($routes) {
        if (!empty($routes[1])) {
            $controller_name = ucfirst(strtolower($routes[1])).'Controller';
            $controller_file = $controller_name.'.php';
            $controller_path = 'application/controllers/'.$controller_file;
            if(file_exists($controller_path)) {
                return $controller_name;
            } else {
                throw new PageNotFoundException();
            }
        } else {
            return null;
        }
    }

    private static function getAction($routes) {
        if (!empty($routes[2]))  {
            $action = reset(explode('?',$routes[2]));
            return $action;
        } else {
           return null;
        }
    }
}
