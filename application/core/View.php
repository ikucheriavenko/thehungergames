<?php

class View {

	function generate($content_view, $template_view, $data = null) {
        $content_style = str_replace('.php','',$content_view).'-style.css';
		include 'application/views/'.$template_view;
	}
}
