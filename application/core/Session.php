<?php
/*
 * set php.ini:
 *  session.save_handler = user;
 *  session.gc_maxlifetime = 1800c,
 *  session.gc_probability = session.gc_divisor = 1 => 100% probability;
 */
class Session {

    private static $session;
    private  $repository;
    private $timeLimit = 60 * 60 * 24;

    private function __construct() {
        $this->repository = new SessionRepositoryImpl();
    }

    public static function getInstance() {
        if (self::$session == null) {
            self::$session = new Session();
        }
        return self::$session;
    }

    public function open() {
        $limit = time() - ($this->timeLimit);
        return $this->repository->deleteOld($limit);
    }

    public function read($id) {
        return $this->repository->get($id);
    }

    public function write($id, $data) {
        return $this->repository->update($id, $data);
    }
    /*
     *  using one db connection per app - can't close it.
     *  another external source isn't used
     */
    public function close() {
        return true;
    }

    public function destroy($id) {
        return $this->repository->delete($id);
    }

    public function gc($maxlifetime) {
        $session_time = time() - intval($maxlifetime);
        return $this->repository->deleteOld($session_time);
    }

}