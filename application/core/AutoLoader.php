<?php
ini_set('display_errors', 1);
AutoLoader::init();
class AutoLoader {
    protected static $fileExt = '.php';

    protected static $pathTop;

    protected static $fileIterator = null;

    static function init() {
        static::$pathTop = $_SERVER["DOCUMENT_ROOT"];
    }

    public static function loader($className) {

        $directory = new RecursiveDirectoryIterator(static::$pathTop, RecursiveDirectoryIterator::SKIP_DOTS);
        if (is_null(static::$fileIterator)) {
            static::$fileIterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::LEAVES_ONLY);
        }
        $filename = $className . static::$fileExt;
        foreach (static::$fileIterator as $file) {
            if (strtolower($file->getFilename()) === strtolower($filename)) {
                if ($file->isReadable()) {
                    include_once $file->getPathname();
                }
                break;
            }
        }
    }

    public static function setPath($path) {
        static::$pathTop = $path;
    }
}