<?php

final class DBConnection {

    private static $connection;

    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new DBConnection();
        }
        return $inst;
    }

    private function __construct() {
        require_once('DBconfig.php');

        self::$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABSE);
        if (self::$connection-> connect_error) {
            die("Connection failed: " . self::$connection-> connect_error);
        }
        return self::$connection;
    }

    public function getConnection() {
       return self::$connection;
    }
    public function close(){
        self:: $connection->close();
    }
}