<?php

class FileUploadException extends Exception{
    public function __construct($message = null) {
        parent::__construct($message);
        error_log($message);
    }
}