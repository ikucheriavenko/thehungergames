<?php

class FileUploadUtil {

    const SUPPORTED_FORMATS = ['image/png','image/jpeg','application/pdf'];
    const MAX_SIZE = 1000000;  // 1 mb
    private  $filePath;
    private $fileName;
    private $fileExt;

    public function __construct() {
        $this->filePath = $_SERVER["DOCUMENT_ROOT"]."/application/uploads/";
    }

    public function uploadFile() {
        try {
            if ($this->checkFile()) {
                $this->fileName = time();
                // get file's extension
                $name = $_FILES["uploader"]["name"];
                $var = (explode(".", $name));
                $this->fileExt = strtolower(array_pop($var));

                if (!move_uploaded_file($_FILES['uploader']['tmp_name'],
                    sprintf($this->filePath."%s".".$this->fileExt",
                        $this->fileName))
                ) {
                    throw new Exception('Failed to move uploaded file.');
                }
            }
        } catch(FileUploadException $exception) {
            return $exception;

        } catch(Exception $e) {
            throw new InternalServerException($e->getMessage());
        }

        return $this->fileName;
    }

    private function checkFile() {
        switch ($_FILES['uploader']['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                return false;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new FileUploadException("Exceeded file's size limit");
            default:
                throw new Exception('Unknown errors.');
        }
        // You should also check file's size here.
        if ($_FILES['uploader']['size'] > self::MAX_SIZE) {
            throw new FileUploadException("Exceeded file's size limit.");
        }
        if (!in_array($_FILES['uploader']['type'],self::SUPPORTED_FORMATS)) {
            throw new FileUploadException("Invalid file format");
        }
        return true;
    }

    public function getFileName() {
        return $this->fileName;
    }

    public function getFileExtension() {
        return $this->fileExt;
    }

    public function specifyPath($specificPath) {
        if (!file_exists($this->filePath.$specificPath)) {
            mkdir($this->filePath.$specificPath, 0777, true);
        }
        $this->filePath = $this->filePath.$specificPath;
    }

    public function deletePreviousAvatar($new_avatar) {
        $files = glob($this->filePath . '*');
        foreach ($files as $file) {
            if ((is_file($file)) && ($file != $this->filePath . $new_avatar)) {
                unlink($file);
            }
        }
    }

}