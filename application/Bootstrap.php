<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/application/core/AutoLoader.php';

class Bootstrap {

    public static function init() {
        spl_autoload_register('AutoLoader::loader');
        set_exception_handler(array('ExceptionHandler', 'catchAllException'));
        self::initSession();
        Router::start();
    }

    private static function initSession() {
        $session = Session::getInstance();
        session_set_save_handler(
            array($session, 'open'),
            array($session, 'close'),
            array($session, 'read'),
            array($session, 'write'),
            array($session, 'destroy'),
            array($session, 'gc'));

        register_shutdown_function('session_write_close');
        session_start();
    }
}

