<?php

class ProfileModel extends Model{

    private $userRepository;
    private $errors;
    private $fileUploader;

    public function __construct() {
        $this->userRepository = new UserRepositoryImpl();
        $this->fileUploader = new FileUploadUtil();
    }

    public function editUserProfile() {
        if(!$this->validatePOST()) {
            return false;
        }
        $user_id = $this->userRepository->getId($_SESSION['user']);
        $this->fileUploader->specifyPath("$user_id/");
        $result = $this->fileUploader->uploadFile();

        if ($result instanceof FileUploadException) {
            $this->errors = $result->getMessage();
            return false;
        }
        $this->userRepository->update($user_id, $_POST['name'], $_POST['last_name'],
            $_POST['email'], $_POST['address'],$result.'.'.$this->fileUploader->getFileExtension());
        $this->fileUploader->deletePreviousAvatar
                                ($result.'.'.$this->fileUploader->getFileExtension());
        return true;
    }

    private function validatePOST() {
        foreach ($_POST as $key => $value) {
            if (empty($value) || ($value == $_SESSION['user'])) {  // somebody try to destroy form
                $this->errors =  "$key is empty";
                return false;
            }
        }
        return true;
    }

    public function getErrors() {
        return $this->errors;
    }

}