<?php

class RegistrationModel extends Model{

    private $repository;
    private $fileUploader;
    private $isSaved;
    private $error;

   public function __construct() {
        $this->repository = new UserRepositoryImpl();
        $this->fileUploader = new FileUploadUtil();
    }

    private function createUser() {
        $user = new User();
        $user->setPhone($_POST['phone']);
        $user->setPassword($_POST['password']);
        $this->repository->add($user);
        $this->isSaved = true;
    }

    public function signUp() {
        foreach ($_POST as $key => $value) {
            if (empty($value)){
                $this->error = "$key is empty";
            }
        }
        $isLoginExist = $this->checkDuplication();
        if ($isLoginExist) {
            $this->error = "This phone number is already exists";
        }
        if ($this->error == null) {
            $this->createUser();
        }
    }

    private function checkDuplication() {
        if ($this->repository->getId($_POST['phone']) != null) {
            return true;
        }
        return false;
    }

    public function getError() {
        return $this->error;
    }

    public function IsSaved() {
        return $this->isSaved;
    }

}