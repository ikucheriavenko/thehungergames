<?php

class LogoutModel extends Model {

    public function logout() {
        unset($_SESSION['user']);
        session_destroy();
    }
}