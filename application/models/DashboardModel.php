<?php

class DashboardModel extends Model {

    private $newsRepository;
    private $carRepository;
    private $serviceRepository;
    private $user_data;

    public function __construct() {
        $this->newsRepository = new NewsRepositoryImpl();
        $this->carRepository = new CarRepositoryImpl();
        $this->serviceRepository = new ServiceRepositoryImpl();
        $this->user_data = array();
    }

    private function getNews() {
        $this->user_data['news'] =
            $this->newsRepository->get($this->random());
    }
    /*
     * randomly choose id to news table.
     * 3 news column = 3 different news
     */
    private function random() {
        return array(rand(1, 2),rand(3, 4),rand(5, 6));
    }

    public function getUserData() {
        $this->getNews();
        $current_user = $_SESSION['user'];

        $cars = $this->getUserCars($current_user);
        if (null == $cars) {
            $this->user_data['no_car_message'] = true;
            return $this->user_data;
        }

        $cars = array_slice($cars,0,3);  // user has to know only 3 last position of his data

        $services = $this->getUserServices($current_user);
        if (null == $services) {
            $this->user_data['no_service_message'] = true;
            $this->user_data['cars']= $cars;
            return $this->user_data;
        }

        $this->user_data['cars'] = $cars;
        $services = array_slice($services,0,3);
        $this->user_data['services'] = $services;
        return $this->user_data;
    }

    private function getUserCars($current_user) {
        return $this->carRepository->get($current_user);
    }

    private function getUserServices($current_user) {
        return $this->serviceRepository->get($current_user);
    }

}