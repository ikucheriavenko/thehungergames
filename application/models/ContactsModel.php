<?php

class ContactsModel extends Model{

    private $fileUploader;
    private $userRepository;

    public function __construct() {
        $this->fileUploader = new FileUploadUtil();
        $this->userRepository = new UserRepositoryImpl();
    }

    function createFeedback() {
        if ((!empty($_POST['subject'])) && (!empty($_POST['message']))) {
            $user_id = $this->userRepository->getId($_SESSION['user']);
            $date = date("jMY");
            $this->fileUploader->specifyPath("$user_id/$date/");
            $result = $this->fileUploader->uploadFile();
            if ($result instanceof FileUploadException) {
                return $result->getMessage();
            }
            $this->userRepository->
                addUserMessage($_POST['subject'], $_POST['message'], $user_id, $result);
        }
        return null;
    }



}