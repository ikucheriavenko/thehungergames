<?php

class CarsModel extends Model {
    private $carRepository;
    private $userRepository;
    private $serviceRepository;
    private $error;

    public function __construct() {
        $this->carRepository = new CarRepositoryImpl();
        $this->userRepository = new UserRepositoryImpl();
        $this->serviceRepository = new ServiceRepositoryImpl();
    }

    public function getUserCars() {
       return $this->carRepository->get($_SESSION['user']);
    }

    public function getCars() {
        return json_encode($this->carRepository->getAll());
    }

    public function getCarById() {
        $user_car_id = null;
        if (empty($_POST)) {
            $user_car_id = intval($_GET['id']);
        } else {
            $user_car_id = intval($_POST['user_car_id']);
        }
        return $this->carRepository->getById($user_car_id);
    }

    public function addCar() {
        if (!$this->isPostValid()) {
            return false;
        }
        $user_id = $this->userRepository->getId($_SESSION['user']);
        $date = date("jMY");
        $this->carRepository->
                            add($user_id, intval($_POST['car_id']),$_POST['plate'], $date);
        return true;
    }

    public function deleteCar() {
        $this->serviceRepository->purge(intval($_GET['id']));
        $this->carRepository->delete(intval($_GET['id']));
    }

    public function editCar() {
        if (!$this->isPostValid()) {
            return false;
        }
        // check if this user is an owner of car with user_car_id
        if (!$this->isMatched()) {
            throw new InternalServerException('Unauthorized attempt to edit car');
        }
        $date = date("jMY");
        $this->carRepository->update($_POST['user_car_id'],$_POST['car_id'],$_POST['plate'],$date);
        return true;
    }

    private function isMatched() {
        $user_cars = $this->carRepository->get($_SESSION['user']);
        foreach ($user_cars as $user_car) {
            if ($user_car['id'] == $_POST['user_car_id']){
                return true;
            }
        }
        return false;
    }

    private function isPostValid() {
        foreach ($_POST as $key => $value) {
            if (empty($value)){
                $this->error = "$key is empty";
                return false;
            }
        }
        return true;
    }

    public function getError() {
        return $this->error;
    }


}