<?php

class ServicesModel extends Model{

    private $serviceRepository;
    private $carRepository;
    private $errors;
    private $timetable;
    private $orderSaved = true;
    private $expiration = 2;

    public function __construct() {
        $this->serviceRepository = new ServiceRepositoryImpl();
        $this->carRepository = new CarRepositoryImpl();
        $this->initOpeningTimes();
    }

    public function getUserServices() {
        return $this->serviceRepository->get($_SESSION['user']);
    }

    public function prepareNewOrder() {
        $user_cars = $this->carRepository->get($_SESSION['user']);
        $services = $this->serviceRepository->getAll();
        $result = array('user_cars' => json_encode($user_cars),
                        'services'=> json_encode($services));
        if (!empty($this->errors)) {
            $result['errors'] = $this->errors;
        }
        return $result;
    }

    public function isOrderValid() {
        if (!$this->isPostValid()) {
            return false;
        }
        if(!$this->isDateValid()) {
            $this->errors = 'The wrong date/date format!';
            return false;
        }
        // service's id that user try to edit
        if (!empty($_POST['user_service_id'])) {
            $this->errors = $this->errors.'Editing service failed. Try again';
        }
        return true;
    }

    public function clarifyTheOrder() {
        $date = $this->formatDate();

        $type_id = intval(reset(explode('/',$_POST['type_id/user_car_id'])));
        $user_car_id = intval(end(explode('/',$_POST['type_id/user_car_id'])));
        $service_id = intval($_POST['service_id']);

        $price = $this->serviceRepository->
        getPrice($service_id, $type_id);

        $free_times = $this->getFreeTimes($date);

        $about_service = $this->serviceRepository->getService($service_id);

        $result = array('date' => $date,
            'time'=> json_encode($free_times),
            'service_id' => $service_id,
            'price' => $price,
            'user_car_id' => $user_car_id,
            'about_service' => $about_service);
        return $result;
    }

    public function saveOrder() {
        if (!$this->isPostValid()) {
            $this->orderSaved = false;
        }
        if (!$this->isTimeFree()) {
            $this->orderSaved = false;
        }

        // user add a new service
        if (($this->orderSaved) && (empty($_POST['user_service_id']))) {
            $this->serviceRepository->
                    save(intval($_POST['service_id']), $_POST['date'],
                        intval($_POST['user_car_id']), $_POST['time']);
        }

        if (($this->orderSaved) && (!empty($_POST['user_service_id']))) {
            $this->serviceRepository->
                    update(intval($_POST['user_service_id']),intval($_POST['service_id']),
                            $_POST['date'], intval($_POST['user_car_id']), $_POST['time']);
        }
    }

    public function isOrderSaved() {
        return $this->orderSaved;
    }

    private function isPostValid() {
        foreach ($_POST as $key => $value) {
            if (empty($value) && ($key!='user_service_id')) {
                $this->errors =  "$key is empty";
                return false;
            }
        }
        return true;
    }

    public function cancelOrder() {
        $result = $this->serviceRepository->getUserServiceTime(intval($_GET['id']));

        if (date("jFY") != $result['date']) {
            $this->serviceRepository->delete($_GET['id']);
            return true;
        }
        if (abs(date('H', time()) - $result['time']) > $this->expiration) {
            $this->serviceRepository->delete($_GET['id']);
            return true;
        }

        return false;
    }

    public function isOrderEditable() {
        $result = $this->serviceRepository->getUserServiceTime(intval($_GET['id']));
        if (date("jFY") != $result['date']) {
            return true;
        }
        if (abs(date('H', time()) - $result['time']) > $this->expiration) {
            return true;
        }
        return false;
    }


    private function isDateValid() {
        $date_parts = explode("/", $_POST['date']);
        if (count($date_parts) == 1) {
            return false;
        }
        if ((intval($date_parts[0]) > 31) || (intval($date_parts[0])< 1)) {
            return false;
        }
        if ((intval($date_parts[1]) > 12) || (intval($date_parts[1])< 1)) {
            return false;
        }
        if ((intval($date_parts[2]) > 2017) || (intval($date_parts[2])< 2015)) {
            return false;
        }
        return true;
    }

    private function isTimeFree() {
        $free_times = $this->getFreeTimes($_POST['date']);
        foreach ($free_times as $time) {
            if ($time == $_POST['time']) {
                return true;
            }
        }
        $this->errors ='Sorry, this time is busy, choose another';
        return false;
    }

    private function getFreeTimes($date) {
        $busy_times = $this->serviceRepository->getTimes($date);
        if ($busy_times != null) {
            return array_values(array_diff($this->timetable, $busy_times));
        }
        return $this->timetable;
    }

    private function formatDate() {
        $date_parts = explode("/", $_POST['date']);
        $month_name = date("F",mktime(0,0,0,$date_parts[1],1,2015));
        $month_number = $date_parts[0];
        // if number of month was filled like '08' transform to '8'
        if ((intval($month_number) < 10) && (strlen($month_number)== 2)) {
            $month_number = substr($month_number, 1);
        }
        return $month_number.$month_name.$date_parts[2]; // ..end year
    }

    public function getErrors() {
        return $this->errors;
    }

    private function initOpeningTimes() {
        $this->timetable = array();
        $start_time = 7; // opening times from 7.00 to 23.00
        $close_time = 23;
        for ($hour = $start_time; $hour <= $close_time; $hour++) {
            array_push($this->timetable,"$hour");
        }
    }



}