<?php

class ErrorModel extends Model {

    public function getNotFound() {
        return array('404','Page is not available');
    }

    public function getInternalException() {
        return array('500','Our service is temporarily unavailable');
    }
}