<?php

class LoginModel extends Model{
    private $repository;
    private $isAuthorized;
    private $error;

    function __construct() {
        $this->repository = new UserRepositoryImpl();
    }

    public function logIn() {
        foreach($_POST as $key => $value){
            if(empty($value)){;
                $this->error = "$key is empty";
            }
        }
        if ($this->error == null) {
            $user = $this->getUser();
            if (is_null($user)) {
                $this->error ='Wrong login or password';
            } else {
                $_SESSION['user'] = $user->getPhone();
                $this->isAuthorized = true;
            }
        }
    }

    public function getUser() {
        return $this->repository->get($_POST['phone'],$_POST['password']);
    }

    public function getError() {
        return $this->error;
    }

    public function isAuthorized() {
        return $this->isAuthorized;
    }
}