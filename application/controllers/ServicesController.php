<?php

class ServicesController extends Controller{

    public function __construct() {
        parent::__construct();
        $this->model = new ServicesModel();
    }

    function actionIndex() {
        $this->view->generate('services.php','template.php', $data = $this->model->getUserServices());
    }

    function newOrder() {
        $this->view->generate('add_service.php','template.php',$this->model->prepareNewOrder());
    }

    function chooseTime() {
        if ((isset($_POST) && $_POST)) {
            if (!$this->model->isOrderValid()) {
                $this->newOrder();
            } else {
                $this->view->generate('add_service_time.php','template.php',$this->model->clarifyTheOrder());
            }
        } else {
            $this->newOrder();
        }
    }

    function saveOrder() {
        $this->model->saveOrder();
        if ($this->model->isOrderSaved()) {
            $this->view->generate('add_service_success.php','template.php');
        } else {
            $this->newOrder();
        }
    }

    function cancel() {
        if ($this->model->cancelOrder()) {
            $this->redirect('/services');
            exit();
        }
        $_GET['error'] = true;
        $this->actionIndex();
    }

    function edit() {
        if (!$this->model->isOrderEditable()) {
            $_GET['error'] = true;
            $this->actionIndex();
        } else {
            $this->newOrder();
        }
    }

}