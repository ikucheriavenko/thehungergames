<?php

class CarsController extends Controller{

    public function __construct() {
        parent::__construct();
        $this->model = new CarsModel();
    }

    function actionIndex() {
        $this->view->generate('cars.php','template.php',$this->model->getUserCars());
    }

    function add() {
        if ((isset($_POST) && $_POST)) {
            if ($this->model->addCar()) {
                $this->redirect('/cars');
                exit();
            }
            $data = array(
                'error' => $this->model->getError(),
                    'cars_list' => $this->model->getCars());

            $this->view->generate('add_car.php','template.php', $data);
        } else {
            $data = array('cars_list'=>$this->model->getCars());
            $this->view->generate('add_car.php','template.php', $data);
        }
    }

    function delete() {
        $this->model->deleteCar();
        $this->redirect('/cars');
    }

    function edit() {
        if ((isset($_POST) && $_POST)) {
            if ($this->model->editCar()) {
                $this->redirect('/cars');
                exit();
            }
            $data = array('error' => $this->model->getError(),
                        'car_for_edit' => $this->model->getCarById(),
                        'cars_list' => $this->model->getCars());
            $this->view->generate('edit_car.php', 'template.php', $data);
        }
        $car = $this->model->getCarById();
        $data = array('car_for_edit' => $car,
                      'cars_list' => $this->model->getCars());
        $this->view->generate('edit_car.php','template.php', $data);
    }

}