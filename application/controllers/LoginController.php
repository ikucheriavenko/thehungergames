<?php

class LoginController extends Controller {

    public function __construct() {
        parent::__construct();
        $this->model = new LoginModel();
    }

    function actionIndex() {
        if (isset($_SESSION['user'])) {
            $this->redirect('/dashboard');
        }
        if (isset($_POST) && $_POST) {
            $this->model->logIn();
            if ($this->model->isAuthorized()) {
                $this->redirect('/dashboard');
            } else {
                $this->view->generate(null,'login_form.php',$this->model->getError());
            }
        } else {
            $this->view->generate(null,'login_form.php');
        }
    }


}