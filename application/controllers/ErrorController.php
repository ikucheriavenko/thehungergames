<?php

class ErrorController extends Controller {

    public function __construct() {
        parent::__construct();
        $this->model = new ErrorModel();
    }

    function actionIndex() {
        $this->notFound();
    }

    function notFound() {
        $this->view->generate(null, 'error_page.php',$this->model->getNotFound());
    }

    function internalException () {
        $this->view->generate(null, 'error_page.php',$this->model->getInternalException());
    }
}