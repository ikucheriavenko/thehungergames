<?php

class DashboardController extends Controller{

    public function __construct() {
        parent::__construct();
        $this->model = new DashboardModel();
    }

    function actionIndex() {
        $this->view->generate('dashboard.php', 'template.php', $this->model->getUserData());
    }
}