<?php

class LogoutController  extends Controller{

    public function __construct() {
        parent::__construct();
        $this->model = new LogoutModel();
    }

    function actionIndex() {
        $this->model->logout();
        $this->redirect('/');
    }
}