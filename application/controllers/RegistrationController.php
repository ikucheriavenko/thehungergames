<?php

class RegistrationController extends Controller {

    public function __construct() {
        parent::__construct();
        $this->model = new RegistrationModel();
    }

    public function actionIndex() {
        if (isset($_POST) && $_POST) {
            $this->model->signUp();
            if ($this->model->isSaved()) {
                $this->redirect('/login');
            } else {
                $this->view->generate(null,'registration_form.php',$this->model->getError());
            }
        } else {
            $this->view->generate(null,'registration_form.php');
        }
    }

}