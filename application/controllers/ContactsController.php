<?php

class ContactsController extends Controller{

    public function __construct() {
        parent::__construct();
        $this->model = new ContactsModel();
    }

    function actionIndex() {
        if (isset($_POST) && $_POST) {
            $error_message = $this->model->createFeedback();
            if ($error_message != null) {
                $this->view->generate('contacts.php', 'template.php',$error_message);
            } else {
                $this->redirect('/dashboard');
            }
        } else {
            $this->view->generate('contacts.php', 'template.php');
        }
    }
}