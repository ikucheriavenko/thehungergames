<?php

class ProfileController extends Controller{

    public function __construct() {
        parent::__construct();
        $this->model = new ProfileModel();
    }

    function actionIndex() {
        if (isset($_POST) && $_POST) {
            if ($this->model->editUserProfile()) {
                $this->redirect('/');
                exit();
            }
            $this->view->generate('profile.php','template.php', $this->model->getErrors());
        } else {
            $this->view->generate('profile.php','template.php');
        }
    }

}