## TheHungerGames ##

App CarWash represent an MVC app with one entry point - index.php (due to .htaccess configuration). 
Start class - class Bootstrap. It register session handler and autoloader (these realization you can find in 'core' directory). Without registration/authorization a casual guest can visit registration/login page. 
All requests send to class Router that choose appropriate controller and action.


### Summary ###

* No jQuery (only for Bootstrap js plugins like 'modal'), MVC pattern, Repository pattern in app's structure.
* carwash_dump.sql in root - to observe tables used.
* 'application' includes php classes and view php files
* 'web' includes all front-end resourses: less, css, js files.
* 'uploads' dir for user's uploaded files - avatars, feedback's files. Inner structure is in accordance with specification.
* v_1