var hours = JSON.parse(document.getElementById('time_schedule').value);
var timesSelect = document.getElementById('time-select');

(function init() {
    var timesSelectLength = timesSelect.options.length;

    for (var k = 0; k < hours.length; k++) {
        timesSelect.options[timesSelectLength++]
            = new Option(hours[k]+'.00', hours[k]);
    }
}());

function getEndTime() {
    var startTime = document.getElementById('time-select').value;
    if (startTime === '') {
        document.getElementById('end-time').innerHTML = '';
    } else {
        document.getElementById('end-time').innerHTML = +startTime + 1 + '.00';
    }
}

function validate() {
    if (timesSelect.value ===  '') {
        document.getElementById('modal-message').innerHTML = 'Please, choose time';
        $('#validation-error').modal('toggle');
        return false;
    }
    return true;
}


