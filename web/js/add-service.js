var cars = JSON.parse(document.getElementById('user_cars').value);
var services = JSON.parse(document.getElementById('services').value);

(function init() {
    var serviceSelect = document.getElementById('service-select');
    var carSelect = document.getElementById('car-select');
    var serviceSelectLength = serviceSelect.options.length;
    var carSelectLength = carSelect.options.length;
    // new service for user's car with this id;
    var car_id = document.getElementById('car_id').value;

    for (var k = 0; k < services.length; k++) {
        var service = services[k];
        serviceSelect.options[serviceSelectLength++]
                            = new Option(service.service_name, service.id);
    }

    for (var j = 0; j < cars.length; j++) {
        var car = cars[j];
        if (car.id === +car_id) {
            changeDefaultOption(car);
        } else {
            carSelect.options[carSelectLength++] =
                new Option(car.mark + ' ' + car.model, car.type_id+'/'+car.id);
        }
    }
}());

function changeDefaultOption(car) {
    var elem = document.getElementById('car-select');
    elem.options[elem.selectedIndex].text = car.mark + ' ' + car.model;
    elem.options[elem.selectedIndex].value = car.type_id+'/'+car.id;
}
