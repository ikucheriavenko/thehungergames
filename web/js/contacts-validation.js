function checkFeedback() {
    var area = document.getElementById('area-msg');
    var subject = document.getElementById('subject');
    if (area.value === '') {
        showError('Field "message" can be empty');
        return false;
    }
    if (subject.value === '') {
        showError('Field "subject" can be empty');
        return false;
    }
    return true;
}

function showError(message) {
    document.getElementById('error-msg').innerHTML="";
    document.getElementById('error-msg').innerHTML = message;
}

function limitLength(element) {
    var max_chars = 20;

    if(element.value.length > max_chars) {
        element.value = element.value.substr(0, max_chars);
    }
}