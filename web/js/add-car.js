var filteredTree ={
    //mark:  -> set of cars with chosen mark
    // model: -> set of cars with early chosen mark and current model
    // etc
};
var cars = JSON.parse(document.getElementById("data_from_server").value);
var carIds = [];

function init() {
    var markObj = document.getElementById('mark');
    var selectLength = markObj.options.length;
    var lastMark = '';

    for (var k = 0; k < cars.length; k++) {
        var car = cars[k];
        if (lastMark != car.mark) {
            markObj.options[selectLength++] =
                new Option(car.mark, car.mark);
            lastMark = car.mark;
        }
    }
}

function openNextSelect(previousSelect,currentSelect,nextSelectName) {
    if (currentSelect.value != "") {

        var nextSelectObj = document.getElementById(nextSelectName);
        // if it's not the last drop-down list - open next or get id of chosen car
        if (nextSelectObj!= null) {
            // delete old values of the next drop-down list
            for (var j = nextSelectObj.options.length-1; j > 0; j--) {
                nextSelectObj.remove(j);
            }
            //delete previous drop-down list's car ids
            carIds = [];
            // fill the next drop-down list
            var nextSelectLength = nextSelectObj.options.length;
            var carsForNextSelect = [];
            var filteredCars = previousSelect in filteredTree ? filteredCars = filteredTree[previousSelect]
                                                              : filteredCars = cars;

            for (var i = 0; i < filteredCars.length; i++) {
                if (currentSelect.value == filteredCars[i][currentSelect.name]) {
                    nextSelectObj.options[nextSelectLength++] =
                        new Option(filteredCars[i][nextSelectName],filteredCars[i][nextSelectName]);
                    carsForNextSelect.push(filteredCars[i]);
                    carIds.push(filteredCars[i].id);
                }
            }

            filteredTree[currentSelect.name] = carsForNextSelect;
            openList(nextSelectName);
        } else {
            document.getElementById("car_id").value = carIds;
        }
    } else {
        closeList(currentSelect.name);
    }
}


function openList(elementName) {
    var selectContainer = document.getElementById(elementName +'-drop');
    selectContainer.style.display = 'block';
}

function closeList(elementName) {
    var items = document.getElementsByClassName('drop');
    var indexOfElement;

    for (var i = 0; i < items.length; i++) {
        if (items[i] === document.getElementById(elementName + '-drop')) {
            indexOfElement = i;
            break;
        }
    }

    for (var k = indexOfElement+1; k < items.length; k++ ) {
        items[k].style.display = 'none';
    }
}

