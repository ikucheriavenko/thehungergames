function limitLength(element, max) {
    if(element.value.length > max) {
        element.value = element.value.substr(0, max);
    }
}

function emailValidation() {
    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regex.test(document.getElementById('email').value)) {
        document.getElementById('error-msg').innerHTML = 'email is not valid';
        return false;
    }
    return true;
}

function previewImg(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            document.getElementById('preview').src = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }

}
