function validateCar() {
    var formElements = ['mark','model','plate','year','type'];
    for (var i = 0; i < formElements.length; i++) {
        if (!checkUp(formElements[i])) { return false;}
    }
    return true;
}

function checkUp(element) {
    var formElement = document.getElementsByName(element)[0];
    if (formElement.value === '') {
        showError(formElement.name +' is empty');
        return false;
    }
    if (formElement.name === 'plate') {
        if (formElement.value.length < 8) {
            showError(formElement.name + ' can not be less than 8 characters');
            return false;
        }
    }
    return true;
}

function showError(message) {
    document.getElementById('error-msg').innerHTML="";
    document.getElementById('error-msg').innerHTML = 'Field  ' + message;
}