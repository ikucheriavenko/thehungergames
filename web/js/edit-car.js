var resetData = function() {
    resetData = function(){}; // call once and never again

    document.getElementById('model-drop').style.display = 'none';
    document.getElementById('year-drop').style.display = 'none';
    document.getElementById('type-drop').style.display = 'none';
    document.getElementById('mark').options[0].text = '-- no chosen --';
    document.getElementById('model').options[0].text = '-- no chosen --';
    document.getElementById('year').options[0].text = '-- no chosen --';
    document.getElementById('type').options[0].text = '-- no chosen --';
    init();  // from add-car.js
};


