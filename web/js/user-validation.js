function validateForm(form) {
    var regex = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,9}$/; // russian + ukrainian

    if (!form.phone.value) {
        showError('telephone number', 'is empty');
        return false;
    }
    if (!regex.test(form.phone.value)) {
        showError('telephone number', 'has the wrong format');
        return false;
    }
    if (!form.password.value) {
        showError('password', 'is empty');
        return false;
    }
    if (form.password.value.length < 6) {
        showError('password', 'can not be shorter than 6 symbols');
        return false;
    }
    return true;
}

function showError(element, message) {
    document.getElementById('error-msg').innerHTML="";
    document.getElementById('error-msg').innerHTML = 'Field  '+ element + ' ' + message;
}

function limitLength(element, max) {
    if(element.value.length > max) {
        element.value = element.value.substr(0, max);
    }
}