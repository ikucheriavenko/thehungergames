function dateValidate() {
    var pattern =/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})$/;
    var input = document.getElementById('date').value;
    var serviceSelect = document.getElementById('service-select').value;
    var carSelect = document.getElementById('car-select').value;

    if (input === '') {
        showError('Please, fill date');
        $('#validation-error').modal('toggle');
        return false;
    }

    if (serviceSelect ===  '') {
        showError('Please, choose service');
        $('#validation-error').modal('toggle');
        return false;
    }

    if (carSelect === '') {
        showError('Please, choose car');
        $('#validation-error').modal('toggle');
        return false;
    }
    /////////////////////////////////
    var dateParts = input.split('/');
    var test = pattern.test(input);
    // user use backslash or any dividers
    if ((test === false) && (dateParts.length === 1)) {
        showError('Use slash("/") to split day, month, year');
        $('#validation-error').modal('toggle');
        return false;
    }

    if (test === false) {
        showError('The wrong date format');
        $('#validation-error').modal('toggle');
        return false;
    }
    /////////////////////////////////
    var day = dateParts[0];
    var month = dateParts[1];
    var year = dateParts[2];

    if ((day < 1)|| (day > 31)) {
        showError('The wrong number of month, dummy');
        $('#validation-error').modal('toggle');
        return false;
    }

    if ((month < 1)|| (month > 12)) {
        showError('There is not ' + month + ' th month!');
        $('#validation-error').modal('toggle');
        return false;
    }

    if ((year < 2015)|| (year > 2017)) {
        showError('Wow-wow-wow, take it easy! The wrong year!');
        $('#validation-error').modal('toggle');
        return false;
    }


    return true;

}

function showError(message) {
    document.getElementById('modal-message').innerHTML = '';
    document.getElementById('modal-message').innerHTML = message;
}