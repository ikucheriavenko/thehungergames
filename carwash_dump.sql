-- MySQL dump 10.13  Distrib 5.6.25, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: carwash
-- ------------------------------------------------------
-- Server version	5.6.25-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mark` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `year` varchar(20) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_type` (`type_id`),
  CONSTRAINT `fk_type` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars`
--

LOCK TABLES `cars` WRITE;
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` VALUES (1,'Audi','A3','2013',2),(2,'Audi','A4','2011',2),(3,'Bentley','Continental GT','2014',1),(4,'Bentley','Mulsanne','2013',1),(5,'BMW','Touring(F31)','2015',1),(6,'BMW','Couple(E92)','2009',1),(7,'Chevrolet','Aveo','2012',1),(8,'Chevrolet','Captiva','2010',3),(9,'Chevrolet','Orlando','2013',4),(10,'Ford','Fiesta','2012',2),(11,'Ford','Focus','2014',1),(12,'Ford','Kuga','2012',3),(13,'Hyundai','Accent','2013',2),(14,'Hyundai','Elantra','2014',1),(15,'Lada','Kalina Sport','2013',2),(16,'Lada','Granta','2014',1),(17,'Man','TGS','2010',6),(18,'Mazda','CX-9','2013',3),(19,'Mazda','6','2012',1),(20,'Mercedes-Benz','C-class(C204)','2013',1),(21,'Mercedes-Benz','Vario','2005',6),(22,'Mercedes-Benz','Sprinter','2008',4),(23,'Nissan','Almera','2012',1),(24,'Nissan','Tiida','2011',2),(25,'Nissan','Murano','2010',3),(26,'Porsche','Carrera','2013',5),(27,'Porsche','Boxster','2012',5),(28,'Volkswagen','New Beetle','2011',2),(29,'Volkswagen','Polo','2010',2),(30,'Volkswagen','Crafter','2012',6);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'Plane wreckage','The wreckage of an Indonesian plane...'),(2,'Cosmos news','A planet 100 light-years away changes...'),(3,'Climate change','Climate change is increasing the risk...'),(4,'Alibaba is sold','The fund owned by the billionaire from...'),(5,'Music benefits','Listening to music before an operation...'),(6,'Be fit','Cutting fat from your diet can drops...');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_prices`
--

DROP TABLE IF EXISTS `service_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `price` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `service_prices_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`),
  CONSTRAINT `service_prices_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_prices`
--

LOCK TABLES `service_prices` WRITE;
/*!40000 ALTER TABLE `service_prices` DISABLE KEYS */;
INSERT INTO `service_prices` VALUES (1,1,1,'19.90'),(2,1,2,'17.50'),(3,1,3,'8.80'),(4,1,4,'5.55'),(5,1,5,'14.39'),(6,1,6,'9.99'),(7,1,7,'15.60'),(8,2,1,'19.43'),(9,2,2,'16.30'),(10,2,3,'9.50'),(11,2,4,'6.00'),(12,2,5,'15.00'),(13,2,6,'9.90'),(14,2,7,'16.00'),(15,3,1,'18.45'),(16,3,2,'15.29'),(17,3,3,'8.99'),(18,3,4,'4.55'),(19,3,5,'15.65'),(20,3,6,'10.00'),(21,3,7,'17.00'),(22,4,1,'17.39'),(23,4,2,'14.95'),(24,4,3,'9.99'),(25,4,4,'10.35'),(26,4,5,'12.99'),(27,4,6,'12.00'),(28,4,7,'18.49'),(29,5,1,'18.04'),(30,5,2,'14.69'),(31,5,3,'7.99'),(32,5,4,'7.25'),(33,5,5,'10.89'),(34,5,6,'16.79'),(35,5,7,'19.00'),(36,6,1,'19.99'),(37,6,2,'18.59'),(38,6,3,'9.20'),(39,6,4,'7.30'),(40,6,5,'20.50'),(41,6,6,'5.00'),(42,6,7,'10.00');
/*!40000 ALTER TABLE `service_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(60) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'full service wash','Interior vacuum of carpets and seats.Exterior wash including wheels.Dash, door panel, and console wiped down.'),(2,'exterior wash','Exterior Wash including wheels. Hand-dry by our staff'),(3,'ride-thru exterior wash','Professional exterior cleaning only.'),(4,'polish & sealer','Adds vivid shine to your vehicle\'s surfaces and improves and maintains finish.'),(5,'extra full details','Complete Exterior and Interior Details, Shampoo Seats Carpet and Boot, Engine Wash, Interior Treatment, Hand Polish, Tyre Shine & Air Freshener.'),(6,'tyre service','Pump tyres, hand clean, correct pressure inside'),(7,'interior wash','Professional interior cleaning only.');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(255) NOT NULL,
  `data` mediumtext NOT NULL,
  `time` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('2n70anomji2c5fqf82tsltj4sscuaa7k','user|s:10:\"0988398513\";',1440406656),('4inbq7c741bc2htkvncisl6qfgsm18ot','user|s:10:\"0988398513\";',1440406018),('4mkai4q9o7e129qserru3lc508s61uqb','user|s:10:\"0988398525\";',1439932193),('nal5i4ubd6gaagj104gb03qp8hks310s','user|s:10:\"0988398514\";',1440449124);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_type_name` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `types`
--

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` VALUES (5,'couple'),(3,'crossover'),(2,'hatchback'),(4,'minivan'),(1,'sedan'),(6,'truck');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_cars`
--

DROP TABLE IF EXISTS `user_cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `plate` varchar(20) DEFAULT NULL,
  `added` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `us_idIn` (`user_id`),
  KEY `c_idIn` (`car_id`),
  CONSTRAINT `user_cars_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `user_cars_ibfk_2` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_cars`
--

LOCK TABLES `user_cars` WRITE;
/*!40000 ALTER TABLE `user_cars` DISABLE KEYS */;
INSERT INTO `user_cars` VALUES (6,1,4,'FG 1234 HF','24Aug2015'),(15,1,9,'FD 1234 HF','12Mar2012'),(16,1,3,'FD 1234 HF','12Mar2012'),(17,1,10,'FD 1234 HF','12Mar2012'),(19,1,19,'DF 4455 OV','21Aug2015'),(23,1,1,'BB 7896 DB','23Aug2015'),(24,1,3,'CC 7896 DB','24Aug2015'),(28,3,1,'CC 5643 NB','24Aug2015'),(29,3,4,'SS 7896 DB','24Aug2015'),(30,1,1,'BA 5643 NB','24Aug2015'),(31,1,28,'BB 7896 CC','24Aug2015'),(32,1,16,'LA 5643 NB','24Aug2015'),(33,1,5,'AA 5643 BD','24Aug2015');
/*!40000 ALTER TABLE `user_cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_files`
--

DROP TABLE IF EXISTS `user_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(25) NOT NULL,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `file` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_files_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_files`
--

LOCK TABLES `user_files` WRITE;
/*!40000 ALTER TABLE `user_files` DISABLE KEYS */;
INSERT INTO `user_files` VALUES (1,'Hello message','Hello',1,'1440157356'),(2,'Hint','math',1,'1440173375'),(3,'Hello message','lglg10000 = 0.6',1,'1440360286'),(5,'Hello','How are you?',3,'1440426812');
/*!40000 ALTER TABLE `user_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_services`
--

DROP TABLE IF EXISTS `user_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_car_id` int(11) DEFAULT NULL,
  `service_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL,
  `date` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `us_c_idIn` (`user_car_id`),
  KEY `s_idIn` (`service_id`),
  CONSTRAINT `user_services_ibfk_1` FOREIGN KEY (`user_car_id`) REFERENCES `user_cars` (`id`),
  CONSTRAINT `user_services_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service_prices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_services`
--

LOCK TABLES `user_services` WRITE;
/*!40000 ALTER TABLE `user_services` DISABLE KEYS */;
INSERT INTO `user_services` VALUES (22,15,1,'20','5February2015'),(24,6,1,'8','12February2015'),(25,15,3,'7','23April2015'),(27,28,6,'7','9August2015'),(28,17,4,'10','9August2015'),(29,6,2,'10','14February2015'),(30,30,1,'9','25August2015'),(34,28,3,'23','24August2015');
/*!40000 ALTER TABLE `user_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(30) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(40) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `password` varchar(40) NOT NULL,
  `avatar` varchar(30) DEFAULT NULL,
  `address` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'0988398513','Ivan','Kucheriavenko','ivan.069@mail.ru','212ad1a661e2dddce1ce9fe3ba0bfb49f2155071','1440413149.png','7Zarichniy'),(3,'0988398514','Boris','Borisenko','boris@gmail.com','c9f5c2dd3dbebf860ccbc2e3a11b2c60a758719b','1440426991.png','Berlin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-24 23:51:32
